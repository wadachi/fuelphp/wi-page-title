<?php
/**
 * Wadachi FuelPHP Page Title Package
 *
 * A convenient way to set the page title on a controller.
 *
 * @package    wi-page-title
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * ページ　タイトル　コントローラ　インターフェース
 */
interface Interface_Controller_PageTitle
{
  /**
   * ページ　タイトルのビュー変数名
   */
  const PAGE_TITLE_VIEW_VAR = '_page_title';
}
