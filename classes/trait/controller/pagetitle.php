<?php
/**
 * Wadachi FuelPHP Page Title Package
 *
 * A convenient way to set the page title on a controller.
 *
 * @package    wi-page-title
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

namespace Wi;

/**
 * コントローラのページ　タイトル形質
 */
trait Trait_Controller_PageTitle
{
  /**
   * ページ タイトル
  */
  protected $title = null;

  /**
   * アクション後処理
   * リダイレクトされた時に実行されていないのは
   *
   * @access public
   * @param Fuel\Core\Response $response レスポンス
   * @return Fuel\Core\Response レスポンス
   */
  public function after($response)// <editor-fold defaultstate="collapsed" desc="...">
  {
    if (isset($this->template->content))
    {
      \View::set_global(self::PAGE_TITLE_VIEW_VAR, $this->title ?: $this->get_constant('_PAGE_TITLE'));
    }

    return parent::after($response);
  }// </editor-fold>

  /**
   * 一定の値を取得する
   *
   * @access private
   * @param string $name 一定名
   * @return mixed 一定の値
   */
  private function get_constant($name)// <editor-fold defaultstate="collapsed" desc="...">
  {
    $reflector = new \ReflectionClass(get_class($this));
    return $reflector->getConstant($name);
  }// </editor-fold>
}
