<?php
/**
 * Wadachi FuelPHP Page Title Package
 *
 * A convenient way to set the page title on a controller.
 *
 * @package    wi-page-title
 * @version    1.0.0
 * @author     Wadachi, Inc.
 * @link       http://wdch.jp
 */

\Autoloader::add_classes([
  'Wi\\Interface_Controller_PageTitle' => __DIR__.'/classes/interface/controller/pagetitle.php',
  'Wi\\Trait_Controller_PageTitle' => __DIR__.'/classes/trait/controller/pagetitle.php',
]);
